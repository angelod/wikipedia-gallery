; This file was auto-generated by drush make
core = 7.x

api = 2
projects[drupal][version] = "7.22"

; Modules
projects[views_bulk_operations][version] = "3.1"

projects[admin_menu][version] = "3.0-rc4"

projects[beautytips][version] = "2.0-beta2"

projects[ctools][version] = "1.3"

projects[css_injector][version] = "1.8"

projects[entity][version] = "1.1"

projects[rdfx][version] = "2.0-alpha4"

projects[features][version] = "2.0-rc1"

projects[feeds][version] = "2.0-alpha8"

projects[feeds_jsonpath_parser][version] = "1.0-beta2"

projects[feeds_tamper][version] = "1.0-beta4"

projects[job_scheduler][version] = "2.0-alpha3"

projects[js_injector][version] = "2.0"

projects[libraries][version] = "2.1"

projects[panels][version] = "3.3"

projects[panels_extra_layouts][version] = "2.x-dev"

projects[sparql][version] = "2.0-alpha4"

projects[sparql_views][version] = "2.0-beta1"

projects[views][version] = "3.7"

projects[views_fluidgrid][version] = "1.x-dev"

projects[views_datasource][version] = "1.x-dev"

projects[views_navigation][version] = "1.0-rc1"

projects[visualization][version] = "1.0-alpha1"

; Custom features

projects[sailing_ships][location] = http://packages.cloud.lowtech.gr/fserver

; Themes
projects[adaptivetheme][version] = "3.1"

projects[sky][version] = "3.0-rc1"

; Libraries
; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[ARC2][download][type] = "git"
libraries[ARC2][download][url] = "https://github.com/semsol/arc2.git"
libraries[ARC2][directory_name] = "ARC2"
libraries[ARC2][type] = "library"
